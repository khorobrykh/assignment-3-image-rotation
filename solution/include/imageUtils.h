#include <stdbool.h>
#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_IMAGEUTILS_H
#define IMAGE_TRANSFORMER_IMAGEUTILS_H
struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

long calculatePadding(uint32_t width);

bool initImage(struct image *image, uint64_t width, uint64_t height);

void copyImage(struct image const *source, struct image *dest);

#endif //IMAGE_TRANSFORMER_IMAGEUTILS_H
