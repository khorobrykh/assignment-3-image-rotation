#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

struct image rotate(struct image source, int angle);

#endif //IMAGE_TRANSFORMER_ROTATE_H


