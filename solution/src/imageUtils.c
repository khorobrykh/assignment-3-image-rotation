#include "imageUtils.h"
#include <malloc.h>
#include <stdbool.h>

long calculatePadding(uint32_t width) {
    return (long) (4 - ((width * sizeof(struct pixel)) % 4)) % 4;
}

bool initImage(struct image *image, uint64_t width, uint64_t height) {
    image->data = malloc(sizeof(struct pixel) * width * height);
    if (!image->data) {
        return false;
    }
    image->width = width;
    image->height = height;
    return true;
}

void copyImage(struct image const *source, struct image *dest) {
    for (size_t i = 0; i < source->height; i++) {
        for (size_t j = 0; j < source->width; ++j) {
            dest->data[i * source->width + j] = source->data[i * source->width + j];
        }
    }
}
