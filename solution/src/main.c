#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

#define WRONG_ARGS_COUNT "Wrong args count."
#define WRONG_ANGLE "Wrong angle"
#define FILE_OPENING_ERROR "File opening error"
#define ERROR_EXIT_CODE 1
#define EXIT_CODE 0
#define FILE_WRITE_MODE "wb"
#define FILE_READ_MODE "rb"


int main(int argc, char **argv) {
    if (argc != 4) {
        perror(WRONG_ARGS_COUNT);
        return ERROR_EXIT_CODE;
    }

    FILE *fileIn = fopen(argv[1], FILE_READ_MODE);
    if (fileIn == NULL) {
        perror(FILE_OPENING_ERROR);
        return ERROR_EXIT_CODE;
    }
    struct image img = {0};

    enum read_status rs = from_bmp(fileIn, &img);
    if (rs != READ_OK) {
        perror(readStatusStringRepresentation(rs));
        return ERROR_EXIT_CODE;
    } else {
        printf("%s\n", readStatusStringRepresentation(rs));
    }

    fclose(fileIn);

    struct image result = {0};
    int angle = (int) strtol(argv[3], NULL, 10);

    if (angle == -270 || angle == -180 || angle == -90 || angle == 0 || angle == 90 || angle == 180 || angle == 270) {
        result = rotate(img, angle);
    } else {
        perror(WRONG_ANGLE);
        return ERROR_EXIT_CODE;
    }
    free(img.data);

    FILE *fileOut = fopen(argv[2], FILE_WRITE_MODE);
    if (fileIn == NULL) {
        perror(FILE_OPENING_ERROR);
        return ERROR_EXIT_CODE;
    }
    enum write_status ws = to_bmp(fileOut, &result);

    if (ws != WRITE_OK) {
        perror(writeStatusStringRepresentation(ws));
    } else {
        printf("%s\n", writeStatusStringRepresentation(ws));
    }

    fclose(fileOut);
    free(result.data);
    return EXIT_CODE;
}
